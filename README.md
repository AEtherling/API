# API
DEPLOY:

first step u need to deploy(!):

$ python3 deploy_script.py from Deploy folder

After that u can run $ python3 head_module.py and emulator

ZAMMAD DEPLOY CAN TAKE AROUND 2-5 MIN AFTER COMPOSE!

$ sudo docker-compose down
From Deploy/zammad-docker-compose will turn off all zammad containers

If u want to delete DB - after "docker stop [container]" and "docker rm [container]" you need to delete all migration versions in  Deploy/migrations/versions

----------------------------------------------

All configs listed in .env, which will be located on server, config.py take settings from .env, also config.py can add fallback option (for example if port for app closed or smth else, port in config will be choosen instead)

Data in configs used for DB: 
FLASK APP, FLASK DEBUG, FLASK RUN HOST, SQLALCHEMY DATABASE URI

Data in configs used for emulator:
EMULATOR_HOST, EMULATOR_PORT, EMULATOR_ALL_TICKETS_ENDPOINT, EMULATOR_ADD_COMMENT_ENDPOINT, EMULATOR_GET_COMMENT_ENDPOINT

Data in configs used for Zammad and Zammad webhook:
API_ZAMMAD_PORT, API_ZAMMAD_URL, API_ZAMMAD_USERNAME, API_ZAMMAD_PASSWORD, WEBHOOK_USERNAME, WEBHOOK_PASSWORD
-If you want manually set ur IP for zammad - change API_ZAMMAD_HOST variable

Attachments folder:
ATTACHMENT_FOLDER_NAME_FOR_ARTICLE_HANDLER

PSYCOPG@ connection:
PSYCOPG2_CONNECT

----------------------------------------------

deploy.py contains script for:

-Zammad

-First migration

-Postgres

deployment.

----------------------------------------------

Flask-migration syntax:

flask db [COMMAND ARG]

$ flask db init 
Adds migration folder to app

$ flask db migrate 
Creates first migration version

Also u can add name of ur migrations:
$ flask db migrate -m "Initial migration."

$ flask db upgrade 
Upgrades the database. If revision isn’t given then "head" is assumed.


$ flask db init --multidb 
Adds support for  multiple database's migrations, associated with app

$ flask db revision 
Creates an empty revision script

$ flask db check
Chacks that a migrate command would not generate any changes 

$ flask db downgrade
Downgrades the database. If revision isn’t given then -1 is assumed.

$ flask db current
Shows the current revision of the database.

For additional flask migrate commands:
https://flask-migrate.readthedocs.io/en/latest/#command-reference