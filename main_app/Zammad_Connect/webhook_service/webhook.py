from flask import Flask, jsonify, request
from flask_basicauth import BasicAuth
import base64
from Zammad_Connect.article_handler import get_article
from config.config import *


app_webhook = Flask(__name__)
basic_auth = BasicAuth(app_webhook)

host_port_of_zammad = f'{API_ZAMMAD_HOST}:{API_ZAMMAD_PORT}'
username = WEBHOOK_USERNAME
password = WEBHOOK_PASSWORD

encoded_credentials = base64.b64encode(f'{username}:{password}'.encode("utf-8")).decode("utf-8")


app_webhook.config['BASIC_AUTH_USERNAME'] = username
app_webhook.config['BASIC_AUTH_PASSWORD'] = password


@app_webhook.route('/webhook/data', methods=['POST'])
@basic_auth.required
def handle_webhook():
    data = request.json
    host = request.headers.get('Host')
    user_agent = request.headers.get('User-Agent')
    auth = request.headers.get('Authorization')
    if user_agent == "Zammad User Agent" and auth == f'Basic {encoded_credentials}':
        get_article.check_attachment(data)
        return jsonify(data)
    else:
        return "", 404
    
