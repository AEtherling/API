#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from Zammad_Connect.zammad_api.client_zammand_api import return_client_zammand_api


class TicketArticles:
    def __init__(self, ticket_number: int):
        self.ticket_number = ticket_number

    def return_ticket_articles(self, client: "zammad_py.api.ZammadAPI") -> list:
        return client.ticket.articles(self.ticket_number)

    def main(self) -> (int, list):
        client = return_client_zammand_api()
        return self.ticket_number, self.return_ticket_articles(client)
