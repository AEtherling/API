#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from zammad_py import ZammadAPI
from config.config import *


def return_client_zammand_api() -> "zammad_py.api.ZammadAPI":

    client = ZammadAPI(
                        url=API_ZAMMAD_URL,
                        username=API_ZAMMAD_USERNAME,
                        password=API_ZAMMAD_PASSWORD
    )
    return client
