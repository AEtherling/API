#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from Zammad_Connect.zammad_api.client_zammand_api import return_client_zammand_api


class TicketCreate:
    def __init__(self, *args, data_from_web=False, data_from_hpsm=False):
        self.data_from_web = data_from_web
        self.data_from_hpsm = data_from_hpsm

        if isinstance(args[0], dict):
            if len(args[0]) == 7:
                # data from web
                self.dict_from_web = args[0]
            else:
                # data from hpsm
                self.dict_from_hpsm = args[0]

    def create_ticket(self, client: "zammad_py.api.ZammadAPI"):
        if self.data_from_web is True:
            params = {
                        "title": self.dict_from_web["title"],
                        "group": self.dict_from_web["group"],
                        "customer": self.dict_from_web["customer"],
                        "article": {
                                        "subject": self.dict_from_web["article_subject"],
                                        "body": self.dict_from_web["article_body"],
                                        "type": self.dict_from_web["article_type"],
                                        "internal": self.dict_from_web["article_internal"]
                        }
            }

        if self.data_from_hpsm is True:
            params = {
                        "title": self.dict_from_hpsm["Краткое описание"],
                        "group": self.dict_from_hpsm["Группа"],
                        "customer": self.dict_from_hpsm["Инициатор"],
                        "priority_id": "2", #Когда сделаем приоритет тут будет значение из словаря dict_from_hpsm["Приоритет"]
                        "article": {
                            #"subject": self.dict_from_hpsm["article_subject"],
                            "body": self.dict_from_hpsm["Описание"],
                            #"type": self.dict_from_hpsm["article_type"]
                            #"internal": self.dict_from_hpsm["article_internal"]
               }
            }
        
        new_ticket = client.ticket.create(params=params)
        return new_ticket

    def main(self) -> (int, list):
        client = return_client_zammand_api()
        return self.create_ticket(client)

