from Zammad_Connect.article_handler import download_attachments
from HPSM_Connect.hpsm_api.creating_hpsm_comment import create_comment
from HPSM_Connect.hpsm_api.get_hpsm_comment_id import *
from HPSM_Connect.hpsm_api.creating_hpsm_attachment import send_attachment_to_hpsm
from Deploy.working_with_db.get_hpsm_id_for_creating_comment import get_hpsm_id_from_shared_tickets
from Deploy.working_with_db.adding_info_when_creating_comment import *


#Функция принимает все данные от вебхука и добавляет в словарь необходимые
def check_attachment(data):
    article = {
        "ticket_id": data['article']['ticket_id'],
        "article_id": data['article']['id'],
        "article_created_by": data['article']['created_by']['email'],
        "article_created_at": data['article']['created_at'],
        "article_body": data['article']['body'],   
    }
    hpsm_ticket_id = get_hpsm_id_from_shared_tickets(article["ticket_id"])
    #Проверка на наличие аттачментов, если есть то в словарь attachment добавляется attachment_filename и attachment_url 
    if 'attachments' in data['article'] and len(data['article']['attachments']) > 0:
        for i in range(0, len(data['article']['attachments'])):
            article[f'attachment_filename_{i + 1}'] = data['article']['attachments'][i]['filename']
            article[f'attachment_url_{i + 1}'] = data['article']['attachments'][i]['url']
            article['attachment'] = True
            path_to_attachment = download_attachments.download_attach(article[f'attachment_filename_{i + 1}'], article[f'attachment_url_{i + 1}'], article['ticket_id'], article['article_id'])
            send_attachment_to_hpsm(path_to_attachment, hpsm_ticket_id)
        insert_zammad_comment_id_into_shared_comments(article["ticket_id"], article["article_id"])
        create_comment(hpsm_ticket_id, article["article_body"], article["article_created_by"])
        insert_hpsm_comment_id_into_shared_comments(article["ticket_id"], get_comment_id(parse_json_data(get_comment(hpsm_ticket_id))), article["article_id"])

    else:
        article['attachment'] = False
        insert_zammad_comment_id_into_shared_comments(article["ticket_id"], article["article_id"])
        create_comment(get_hpsm_id_from_shared_tickets(article["ticket_id"]), article["article_body"], article["article_created_by"])
        insert_hpsm_comment_id_into_shared_comments(article["ticket_id"], get_comment_id(parse_json_data(get_comment(hpsm_ticket_id))), article["article_id"])