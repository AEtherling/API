import requests, os
from Zammad_Connect.article_handler.__init__ import username, password, attachment_folder_name, current_directory


def directory_for_tickets_attachment(attachment_folder_name, ticket_id, article_id):
    directory_for_tickets = os.path.join(current_directory, "..", attachment_folder_name, str(ticket_id), str(article_id))
    return(directory_for_tickets)


def filename_generation_if_exists(path_to_dir, filename):
    base_name, extension = os.path.splitext(filename)
    i = 1
    while os.path.exists(f'{path_to_dir}/{base_name}_{i}{extension}'):
        i += 1
    new_filename = f'{base_name}_{i}{extension}'
    return new_filename


def save_file(path_to_file, data):
    with open(path_to_file, "wb") as file:
            file.write(data)
            

def write_attachment_to_fs(path_to_dir, filename, data):
    full_path_to_file = os.path.join(path_to_dir, filename)
    if not os.path.exists(full_path_to_file):
        save_file(full_path_to_file, data)
        return full_path_to_file
    else:
        new_filename = filename_generation_if_exists(path_to_dir, filename)
        full_path_to_new_file = os.path.join(path_to_dir, new_filename)
        save_file(full_path_to_new_file, data)
        return full_path_to_new_file


def download_attach(filename, url, ticket_id, article_id):
    response = requests.get(url, auth=(username, password))
    if not os.path.exists(directory_for_tickets_attachment(attachment_folder_name, ticket_id, article_id)):
        os.makedirs(directory_for_tickets_attachment(attachment_folder_name, ticket_id, article_id))
    if response.status_code == 200:
        get_path = write_attachment_to_fs(directory_for_tickets_attachment(attachment_folder_name, ticket_id, article_id), filename, response.content)
        return get_path
    else:
        return 404, ""