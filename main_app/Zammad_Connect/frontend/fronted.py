#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from flask import Flask
from Zammad_Connect.frontend.config import Configuration
from Zammad_Connect.frontend.get_ticket.blueprint import get_ticket
from Zammad_Connect.frontend.create_ticket.blueprint import create_ticket


app_fronted = Flask(__name__)
app_fronted.config.from_object(Configuration)

app_fronted.register_blueprint(get_ticket, url_prefix="/get_ticket")
app_fronted.register_blueprint(create_ticket, url_prefix="/create_ticket")
