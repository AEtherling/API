#!/usr/bin/env python3
# -*- 	coding: utf-8 -*-


from flask import request
from flask import Blueprint
from flask import render_template
from Zammad_Connect.zammad_api.create_ticket import TicketCreate


create_ticket = Blueprint("create_ticket", __name__, template_folder="templates")

@create_ticket.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        title = request.form["title"]
        group = request.form["group"]
        customer = request.form["customer"]
        article_subject = request.form["article_subject"]
        article_body = request.form["article_body"]
        article_type = request.form["article_type"]
        article_internal = request.form["article_internal"]

        dict = {
                    "title": title,
                    "group": group,
                    "customer": customer,
                    "article_subject": article_subject,
                    "article_body": article_body,
                    "article_type": article_type,
                    "article_internal": article_internal
        }

        TicketCreate(dict, data_from_web=True).main()

    return render_template("create_ticket/index.html")
