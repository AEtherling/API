#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from Zammad_Connect.frontend.fronted import app_fronted
from flask import render_template


@app_fronted.route("/")
def index():
    return render_template("index.html")
