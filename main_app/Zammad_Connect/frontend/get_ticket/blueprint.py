#!/usr/bin/env python3
# -*- 	coding: utf-8 -*-


import requests
from flask import request
from flask import Blueprint
from flask import render_template
from Zammad_Connect.zammad_api.get_ticket import TicketArticles


get_ticket = Blueprint("get_ticket", __name__, template_folder="templates")

@get_ticket.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":

        try:
            ticket_number = int(request.form["ticket_number"])
            ticket, ticket_articles = TicketArticles(ticket_number).main()

            return render_template("completed_ticket/index.html",  number_ticket=ticket, t_a=ticket_articles)

        except ValueError:
            return render_template("value_error_only_numbers/index.html")

        except requests.exceptions.HTTPError:
            return render_template("value_error_ticket_not_found/index.html")

    return render_template("get_ticket/index.html")
