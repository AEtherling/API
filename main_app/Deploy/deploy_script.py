import os, time

os.system("sudo docker run --name db_for_tickets_data -p 5433:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=test4Life -e POSTGRES_DB=db_for_tickets_data -d postgres:latest")
time.sleep(3)
os.system("flask db migrate")
os.system("flask db upgrade")
time.sleep(1)
os.system("git clone https://github.com/zammad/zammad-docker-compose.git")
time.sleep(0.5)
os.system("sudo docker-compose -f ./zammad-docker-compose/docker-compose.override-local.yml -f ./zammad-docker-compose/docker-compose.override.yml -f ./zammad-docker-compose/docker-compose.yml up -d")
