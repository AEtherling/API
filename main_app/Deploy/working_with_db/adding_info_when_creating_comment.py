import psycopg2
from Deploy.working_with_db.__init__ import psycopg2_connect
from Deploy.working_with_db.get_hpsm_id_for_creating_comment import get_hpsm_id_from_shared_comments


conn = psycopg2.connect(psycopg2_connect)
cursor = conn.cursor()


def query_to_db(query, value):
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, value)
            conn.commit()
    except Exception as error:
        print(f"An error occurred: {error}")


def get_num_empty_rows(query, value):
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, value)
            return cursor.rowcount
    except Exception as error:
        print(f"An error occurred: {error}")


def insert_zammad_comment_id_into_shared_comments(zammad_ticket_id, zammad_comment_id):
    hpsm_ticket_id = get_hpsm_id_from_shared_comments(zammad_ticket_id)
    check_num_lines = "SELECT * FROM shared_comments WHERE hpsm_ticket_id = %s AND (zammad_comment_id IS NULL)"
    if get_num_empty_rows(check_num_lines, (hpsm_ticket_id,)) == 0:
        insert_tciket_comments_ids = "INSERT INTO shared_comments (hpsm_ticket_id, zammad_ticket_id, zammad_comment_id) VALUES (%s, %s, %s)"
        query_to_db(insert_tciket_comments_ids, (hpsm_ticket_id, str(zammad_ticket_id), str(zammad_comment_id),))
    else:
        update_zammad_ticket_id = "UPDATE shared_comments SET zammad_comment_id = %s WHERE hpsm_ticket_id = %s"
        query_to_db(update_zammad_ticket_id, (str(zammad_comment_id), hpsm_ticket_id))


def insert_hpsm_comment_id_into_shared_comments(zammad_ticket_id, hpsm_comment_id, zammad_comment_id):
    hpsm_ticket_id = get_hpsm_id_from_shared_comments(zammad_ticket_id)
    check_num_lines = "SELECT * FROM shared_comments WHERE hpsm_ticket_id = %s AND (hpsm_comment_id IS NULL)"
    if get_num_empty_rows(check_num_lines, (str(hpsm_ticket_id),)) == 0:
        insert_tciket_comments_ids = "INSERT INTO shared_comments (hpsm_ticket_id, zammad_ticket_id, hpsm_comment_id) VALUES (%s, %s, %s)"
        query_to_db(insert_tciket_comments_ids, (hpsm_ticket_id, str(zammad_ticket_id), str(hpsm_comment_id),))
    else:
        update_zammad_ticket_id = "UPDATE shared_comments SET hpsm_comment_id = %s WHERE hpsm_ticket_id = %s AND (zammad_comment_id = %s)"
        query_to_db(update_zammad_ticket_id, (str(hpsm_comment_id), hpsm_ticket_id, str(zammad_comment_id)))