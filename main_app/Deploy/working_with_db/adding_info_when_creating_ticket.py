import psycopg2
from Deploy.working_with_db.__init__ import psycopg2_connect

conn = psycopg2.connect(psycopg2_connect)
cursor = conn.cursor()

def query_to_db(query, value):
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, value)
            conn.commit()
    except Exception as error:
        print(f"An error occurred: {error}")


def insert_data_into_shared_tickets(dict_with_data):
    title = dict_with_data['Краткое описание']
    working_group = dict_with_data['Группа']
    customer = dict_with_data['Инициатор']
    description = dict_with_data['Описание']
    priority_id = dict_with_data['Приоретет']
    hpsm_ticket_id = dict_with_data['Ключ']
    query = f"INSERT INTO shared_tickets (title, working_group, customer, description, priority_id, hpsm_key) VALUES (%s, %s, %s, %s, %s, %s)"
    values = (title, working_group, customer, description, priority_id, hpsm_ticket_id)
    query_to_db(query, values)


def insert_data_into_shared_comments(hpsm_ticket_id):
    query = "INSERT INTO shared_comments (hpsm_ticket_id) VALUES (%s)"
    query_to_db(query, (hpsm_ticket_id,))


def insert_zammad_ticketid_to_shared_tickets(zammad_ticket_id, hpsm_ticket_id):
    query = "UPDATE shared_tickets SET id_zammad = %s WHERE hpsm_key = %s"
    query_to_db(query, (zammad_ticket_id, hpsm_ticket_id))


def insert_zammad_ticketid_to_shared_comments(zammad_ticket_id, hpsm_ticket_id):
    query = "UPDATE shared_comments SET zammad_ticket_id = %s WHERE hpsm_ticket_id = %s"
    query_to_db(query, (zammad_ticket_id, hpsm_ticket_id))

