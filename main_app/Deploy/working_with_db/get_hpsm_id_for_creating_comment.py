import psycopg2
from Deploy.working_with_db.__init__ import psycopg2_connect

conn = psycopg2.connect(psycopg2_connect)
cursor = conn.cursor()


def get_hpsm_id_from_shared_tickets(zamamd_ticket_id):
    query = "SELECT hpsm_key FROM shared_tickets WHERE id_zammad = %s"
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, (str(zamamd_ticket_id),))
            data = cursor.fetchone()
        if data:
            return data[0]
        else:
            return None
    except Exception as error:
        print(f"An error occurred: {error}")


def get_hpsm_id_from_shared_comments(zamamd_ticket_id):
    query = "SELECT hpsm_ticket_id FROM shared_comments WHERE zammad_ticket_id = %s"
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, (str(zamamd_ticket_id),))
            data = cursor.fetchone()
        if data:
            return data[0]
        else:
            return None
    except Exception as error:
        print(f"An error occurred: {error}")