from flask import Blueprint
from flask_restful import Api

from Deploy.resources.tables import CommentsList

blueprint = Blueprint("api", __name__, url_prefix="/api")
api = Api(blueprint, errors=blueprint.errorhandler)

api.add_resource(CommentsList, "/shared_comment")
