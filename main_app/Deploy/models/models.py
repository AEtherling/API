from dataclasses import dataclass
from extensions import db

@dataclass
class Comments(db.Model):
    id: int
    hpsm_ticket_id: str
    zammad_ticket_id: str
    hpsm_comment_id: str
    zammad_comment_id: str

    __tablename__ = 'shared_comments'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    hpsm_ticket_id = db.Column(db.String(250))
    zammad_ticket_id = db.Column(db.String(250))
    hpsm_comment_id = db.Column(db.String(250))
    zammad_comment_id = db.Column(db.String(250))

@dataclass
class Tickets(db.Model):
    id: int
    title: str
    working_group: str
    customer: str
    description: str
    priority_id: str
    id_zammad: str
    hpsm_key: str

    __tablename__ = 'shared_tickets'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(500))
    working_group = db.Column(db.String(500))
    customer = db.Column(db.String(500))
    description = db.Column(db.String(500))
    priority_id = db.Column(db.String(500))
    id_zammad = db.Column(db.String(500))
    hpsm_key = db.Column(db.String(500))

@dataclass
class Timer(db.Model):
    id: int
    last_check_time: str

    __tablename__ = 'timer'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    last_check_time = db.Column(db.String(500))