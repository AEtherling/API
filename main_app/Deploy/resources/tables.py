from flask import request, jsonify
from flask_restful import Resource

from extensions import db
from Deploy.models import Comments, Tickets, Timer


class CommentsList(Resource):
    def get(self):
        comments = Comments.query.all()
        return jsonify(results=comments)

    def post(self):
        data = request.json

        comment = Comments(
            zammad_comment__id==data.get("zammad_comment_id"),
            zammad_ticket_id==data.get("zammad_ticket_id"),
        )

        db.session.add(comment)
        db.session.commit()

        return jsonify(msg="Comment created", comment=comment)