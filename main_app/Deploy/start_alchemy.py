from flask import Flask

from Deploy.views import blueprint
from extensions import db, migrate

app_alchemy = Flask(__name__)
app_alchemy.register_blueprint(blueprint=blueprint)
app_alchemy.config.from_object("config.config")

db.init_app(app_alchemy)
migrate.init_app(app_alchemy, db)
