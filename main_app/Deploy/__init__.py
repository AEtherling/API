import os, sys
from config.config import *

current_directory = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_directory)

FLASK_APP = FLASK_APP
FLASK_RUN_HOST=FLASK_RUN_HOST
FLASK_RUN_PORT=FLASK_RUN_PORT
FLASK_DEBUG=FLASK_DEBUG
SQLALCHEMY_DATABASE_URI=SQLALCHEMY_DATABASE_URI