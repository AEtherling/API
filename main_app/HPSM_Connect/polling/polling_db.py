import psycopg2
from HPSM_Connect.polling.__init__ import psycopg2_connect


conn = psycopg2.connect(psycopg2_connect)

cursor = conn.cursor()

def update_data_into_db(time):
    query = f"UPDATE timer SET last_check_time = %s WHERE id = (SELECT MAX(id) FROM timer)"
    values = (time,)
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, values)
            conn.commit()
    except Exception as error:
        print(f"An error occurred: {error}")


def insert_data_into_db(time):
    query = f"INSERT INTO timer (last_check_time) VALUES (%s)"
    values = (time,)
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, values)
            conn.commit()
    except Exception as error:
        print(f"An error occurred: {error}")


def get_last_check_time():
    query = f"SELECT * FROM timer ORDER BY id DESC LIMIT 1;"
    try:
        with conn.cursor() as cursor:
            cursor.execute(query)
            return cursor.fetchone()
    except Exception as error:
        print(f"An error occurred: {error}")