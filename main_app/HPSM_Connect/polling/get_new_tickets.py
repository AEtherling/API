import requests
from datetime import datetime
from threading import Thread
from HPSM_Connect.polling import polling_db, get_endpoint_view_all_tickets
from scheduler.scheduler import schedule_polling_func
from Zammad_Connect.zammad_api.create_ticket import TicketCreate
from Deploy.working_with_db import adding_info_when_creating_ticket

def polling_tickets():
    session = requests.Session()
    request_to_tickets = get_endpoint_view_all_tickets.request_to_list_tickets(session)
    dict_for_tickets = get_endpoint_view_all_tickets.get_all_tickets(request_to_tickets)
    current_time = datetime.now().strftime("%d/%m/%y %H:%M:%S")
    
    try:
        last_check_time = str(polling_db.get_last_check_time()[1])
    except TypeError:
        last_check_time = current_time
        polling_db.insert_data_into_db(last_check_time)

    creation_time_new_tickets = get_endpoint_view_all_tickets.get_time_new(session, dict_for_tickets, get_endpoint_view_all_tickets.get_status_new(dict_for_tickets))
    for key, value in creation_time_new_tickets.items():
        try:
            if value > last_check_time:
                #Добавляем данные в таблицы shared_comments и shared_tickets
                adding_info_when_creating_ticket.insert_data_into_shared_tickets(dict_for_tickets[key])
                adding_info_when_creating_ticket.insert_data_into_shared_comments(key)
                #Создаем тикет в zammad
                dict_with_data = TicketCreate(dict_for_tickets[key], data_from_hpsm=True).main()
                #Добавляем ticket_zammad_id в таблицы shared_comments и shared_tickets после создания тикета в zammad
                adding_info_when_creating_ticket.insert_zammad_ticketid_to_shared_tickets(dict_with_data['id'], key)
                adding_info_when_creating_ticket.insert_zammad_ticketid_to_shared_comments(dict_with_data['id'], key)
                
        except requests.exceptions.HTTPError as error:
            print(error)
        except UnboundLocalError as error:
            print(error)
    polling_db.update_data_into_db(current_time)
    session.close()

def start_thread():
    thread = Thread(target=schedule_polling_func, args=(30, polling_tickets))
    thread.start()