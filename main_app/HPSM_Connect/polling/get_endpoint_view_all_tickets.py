import json
from HPSM_Connect.polling.__init__ import url_to_list_tickets

#Запрос к эндпоинту
def request_to_list_tickets(session):
    req = session.get(url_to_list_tickets)
    text = req.text
    try:
        data = json.loads(text)
        return data
    except json.JSONDecodeError as error:
        return {}

    
#Парсим, возвращаем словарь с тикетами
def get_all_tickets(data):
    if data:
        incidents = data['ditMFSMAPI']['Response'][1]
        dictionary = json.loads(incidents)
        return(dictionary)
    else:
        return {}


#Возвращаем словарь со статусом тикетов "Новое"
def get_status_new(dict):
    status_new = {}
    for i in dict:
        if dict[i]['Статус'] == 'Новое':
            status_new.update({i: {'Статус': dict[i]['Статус']}})
    return status_new


#Возвращаем словарь со временем создания тикета
def get_time_new(session, tickets_data, tickets_with_new_status):
    creation_time = {}
    data = get_all_tickets(request_to_list_tickets(session))
    for key, value in tickets_data.items():
        if key in tickets_with_new_status:
            creation_time.update({key: data[key]['Время новое']})
    return creation_time