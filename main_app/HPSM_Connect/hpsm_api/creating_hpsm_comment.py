import requests
from HPSM_Connect.hpsm_api.__init__ import url_to_endpoint_add_comment


def create_comment(ticket_id, comment, author):
    comment_data = {
        "drop_down": ticket_id,
        "commit": comment,
        "author": author
    }
    try:
        response = requests.post(url_to_endpoint_add_comment, data=comment_data)
        return response
    except requests.exceptions.RequestException as e:
        print("An error occurred:", e)
        return None