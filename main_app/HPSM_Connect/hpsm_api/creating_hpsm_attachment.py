import requests, os
from HPSM_Connect.hpsm_api.__init__ import *


def get_token_attachment(hpsm_ticket_id):
        data = {'action': 'getAttachToken','drop_down': hpsm_ticket_id,'filename': 'Обращение'}
        try:
            attachment_token = requests.post(f'http://{EMULATOR_HOST}:{EMULATOR_PORT}/{EMULATOR_GET_TOKEN_ATTACHMENT_ENDPOINT}', data).json()[hpsm_ticket_id]['ditMFSMAPI']['AttachToken']
            return attachment_token
        except (requests.exceptions.RequestException) as e:
            print("An error occurred:", str(e))
            return None
    

def send_attachment_to_hpsm(path_to_file, hpsm_ticket_id):
    attachment_token = get_token_attachment(hpsm_ticket_id)
    filename = os.path.basename(path_to_file)
    url = f'http://{EMULATOR_HOST}:{EMULATOR_PORT}/{EMULATOR_ADD_ATTACHMENT_ENDPOINT}/{attachment_token}/attachments'
    files = {'file': open(path_to_file, 'rb')}
    headers = {
        'Content-Disposition': f'attachment;filename*=utf-8\'\'{filename.encode("utf-8")}'}
    try:
        response = requests.post(url, files=files, headers=headers)
        return("send_attachment_to_hpsm() - ", response)
    except (requests.exceptions.RequestException) as e:
        print("An error occurred:", str(e))
        return None
    


