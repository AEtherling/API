import requests, json, json5
from HPSM_Connect.hpsm_api.__init__ import url_to_enpoint_get_comment


def get_comment(ticket_id):
    comment_data = {
        "drop_down": ticket_id
    }
    try:
        response = requests.post(url_to_enpoint_get_comment, data=comment_data)
        json_data = response.json()
        return json_data
    except requests.exceptions.RequestException as e:
        print("An error occurred:", e)
        return None
    except json.JSONDecodeError as error:
        print("An error occurred:", error)
        return None


def parse_json_data(json_data):
    comments = json_data['ditMFSMAPI']['Response'][0]
    json_comment_data = json5.loads(comments)
    return json_comment_data


def get_comment_id(json_comment_data):
    key = next(iter(json_comment_data.keys()))
    comment_id = len(json_comment_data[key])
    return comment_id