#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from multiprocessing import Process
from HPSM_Connect.polling import get_new_tickets
from Zammad_Connect.webhook_service.webhook import app_webhook
from Deploy.start_alchemy import app_alchemy
from config.config import API_ZAMMAD_HOST, API_ZAMMAD_PORT


def start_webhook():
    app_webhook.run(
        host=API_ZAMMAD_HOST,
        port=API_ZAMMAD_PORT
        )


def start_tickets_polling():
   get_new_tickets.start_thread()


def start_alchemy_flask():
    app_alchemy.run(
        host=app_alchemy.config.get("FLASK_RUN_HOST"),
        port=app_alchemy.config.get("FLASK_RUN_PORT"),
        )

process_tickets_polling = Process(target=start_tickets_polling)
process_webhook = Process(target=start_webhook)
process_alchemy = Process(target=start_alchemy_flask)

process_tickets_polling.start()
process_webhook.start()
process_alchemy.start()

process_tickets_polling.join()
process_webhook.join()
process_alchemy.join()
