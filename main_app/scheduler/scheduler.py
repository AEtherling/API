import schedule, time

#Takes number seconds and function 
def schedule_polling_func(sec, job):
    schedule.every(sec).seconds.do(job)
    while True:
        schedule.run_pending()
        time.sleep(1)

