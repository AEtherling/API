#!/usr/bin/env python3
# -*- 	coding: utf-8 -*-

from flask import request, Blueprint, render_template
from backend_emulator.working_with_JSON_files.read_in_json import ReadsJson
from backend_emulator.attachment_processing.file_proccessing import FileProccessing
from backend_emulator.attachment_processing.attachment_handler import AttachmentHandler

add_attachment = Blueprint("add_attachment", __name__, template_folder="templates")


@add_attachment.route("/", methods=["GET", "POST"])
def index():
    data_json = ReadsJson(name_file="incidents.json").main()

    if data_json is not False:
        list_key_ticket = list(data_json.keys())

        if request.method == "GET":
                return render_template("add_attachment/index.html", drop_down_list=list_key_ticket)

        if request.method == "POST":
            if 'files[]' not in request.files:
                return 'No files attached', 400

            value = request.form["drop_down"]
            files = request.files.getlist('files[]')

            if value in list_key_ticket:
                data = {
                    'action': 'getAttachToken',
                    'drop_down': value,
                    'filename': 'Обращение'
                }

                for file in files:
                    AttachmentHandler(data, file).main()

            else:
                return "Incident doesn't exist"

            return render_template("add_attachment/index.html", drop_down_list=list_key_ticket)

    else:
        return render_template("404/index.html", data="Нет тикетов!")


@add_attachment.route('/<attach_token>/attachments', methods=["POST"])
def attach_attachment(attach_token):
    file = request.files['file']
    header = request.headers['Content-Disposition']
    result = FileProccessing(file, attach_token, header).main()
    return result
