#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from flask import render_template
from frontend_emulator.app_frontend_emulator import app_frontend_emulator


@app_frontend_emulator.route("/")
def index():
    return render_template("index.html")
