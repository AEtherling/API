#!/usr/bin/env python3
# -*- 	coding: utf-8 -*-


from flask import request
from flask import jsonify
from flask import Blueprint
from flask import render_template
from backend_emulator.working_with_JSON_files.read_in_json import ReadsJson
from backend_emulator.get_requests.get_requests_comment_JSON import GetCommentJson


view_all_comment_emulator_DOC = Blueprint("view_all_comment_emulator_DOC", __name__, template_folder="templates")

@view_all_comment_emulator_DOC.route("/", methods=["GET", "POST"])
def index():
    data_json = ReadsJson(name_file="protocol.json").main()

    if data_json is not False:
        list_key_ticket = list(data_json.keys())

        if request.method == "GET":
            return render_template("view_all_comment_emulator_DOC/index.html", drop_down_list=list_key_ticket)

        if request.method == "POST":
            key_id = request.form["drop_down"]
            return jsonify(GetCommentJson(data_json, key_id).main())

    else:
        return render_template("error_404/index.html"), 404
