#!/usr/bin/env python3
# -*- 	coding: utf-8 -*-


from flask import request
from flask import jsonify
from flask import Blueprint
from flask import render_template
from backend_emulator.working_with_JSON_files.read_in_json import ReadsJson
from backend_emulator.get_requests.get_requests_token_attachment import GetTokenAttachment


get_token_attachment_DOC = Blueprint("get_token_attachment_DOC", __name__, template_folder="templates")

@get_token_attachment_DOC.route("/", methods=["GET", "POST"])
def index():
    data_json = ReadsJson(name_file="incidents.json").main()

    if data_json is False:
        return render_template("no_ticket_for_token_attacment/index.html", data="НЕТ СОЗДАННЫХ ЗАЯВОК!")

    else:
        list_key_ticket = list(data_json.keys())

        if request.method == "POST":
            action = request.form["action"]
            key = request.form["drop_down"]
            filename = request.form["filename"]

            dict_data = {
                            key: {
                                    "ditMFSMAPI": {
                                                    "Action": action,
                                                    "Key": key,
                                                    "Filename": filename
                                    }
                            }
            }

            GetTokenAttachment(key, dict_data).main()

            return jsonify(ReadsJson(name_file="token_attachment.json").main())

        if request.method == "GET":
            return render_template("get_token_attachment_DOC/index.html", drop_down_list=list_key_ticket)
