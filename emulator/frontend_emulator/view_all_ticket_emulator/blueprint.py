#!/usr/bin/env python3
# -*- 	coding: utf-8 -*-


from flask import jsonify
from flask import Blueprint
from flask import render_template
from backend_emulator.working_with_JSON_files.read_in_json import ReadsJson


view_all_ticket_emulator = Blueprint("view_all_ticket_emulator", __name__, template_folder="templates")

@view_all_ticket_emulator.route("/")
def index():
    data_json = ReadsJson(name_file="incidents.json").main()

    if data_json is False:
        return render_template("view_all_ticket_emulator/index.html", data="НЕТ СОЗДАННЫХ ЗАЯВОК!")
    else:
        return jsonify(data_json)
