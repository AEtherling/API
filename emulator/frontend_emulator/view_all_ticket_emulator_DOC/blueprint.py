#!/usr/bin/env python3
# -*- 	coding: utf-8 -*-


from flask import jsonify
from flask import Blueprint
from flask import render_template
from backend_emulator.get_requests.get_requests_ticket_JSON import GetJson
from backend_emulator.working_with_JSON_files.read_in_json import ReadsJson


view_all_ticket_emulator_DOC = Blueprint("view_all_ticket_emulator_DOC", __name__, template_folder="templates")

@view_all_ticket_emulator_DOC.route("/")
def index():
    data_json = ReadsJson(name_file="incidents.json").main()

    if data_json is False:
        return render_template("view_all_ticket_emulator_DOC/index.html", data="НЕТ СОЗДАННЫХ ЗАЯВОК!")
    else:
        return jsonify(GetJson(data_json).main())
