#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from flask import Flask
from frontend_emulator.add_a_comment.blueprint import add_comment
from frontend_emulator.add_attachment.blueprint import add_attachment
from frontend_emulator.create_ticket_emulator.blueprint import create_ticket_emulator
from frontend_emulator.get_token_attachment_DOC.blueprint import get_token_attachment_DOC
from frontend_emulator.view_all_ticket_emulator_DOC.blueprint import view_all_ticket_emulator_DOC
from frontend_emulator.view_all_comment_emulator_DOC.blueprint import view_all_comment_emulator_DOC


app_frontend_emulator = Flask(__name__)

app_frontend_emulator.register_blueprint(add_comment, url_prefix="/add_comment")
app_frontend_emulator.register_blueprint(add_attachment, url_prefix="/add_attachment")
app_frontend_emulator.register_blueprint(create_ticket_emulator, url_prefix="/create_ticket_emulator")
app_frontend_emulator.register_blueprint(get_token_attachment_DOC, url_prefix="/get_token_attachment_DOC")
app_frontend_emulator.register_blueprint(view_all_ticket_emulator_DOC, url_prefix="/view_all_ticket_emulator_DOC")
app_frontend_emulator.register_blueprint(view_all_comment_emulator_DOC, url_prefix="/view_all_comment_emulator_DOC")
