#!/usr/bin/env python3
# -*- 	coding: utf-8 -*-


from flask import url_for
from flask import redirect
from flask import request
from flask import Blueprint
from flask import render_template
from backend_emulator.date_time.return_date_time import date_time
from backend_emulator.working_with_JSON_files.write_in_json import WritersJson
from backend_emulator.ticket_code_generator.generator_examination import Generator


create_ticket_emulator = Blueprint("create_ticket_emulator", __name__, template_folder="templates")

@create_ticket_emulator.route("/", methods=["GET", "POST"])
def index():
    key = Generator().main()

    if request.method == "POST":

        Action = request.form["Action"]
        Filename = request.form["Filename"]

        The_contact_person = request.form["The_contact_person"]
        Contact_persons_email = request.form["Contact_persons_email"]
        Contact_person_phone_number = request.form["Contact_person_phone_number"]
        Initiator = request.form["Initiator"]
        Group = request.form["Group"]
        Short_description = request.form['Short_description']
        Description = request.form["Description"]
        A_priority = request.form["A_priority"]

        dict = {
            key: {
              "Имя файла": Filename,
              "Контактное лицо": The_contact_person,
              "Email контактного лица": Contact_persons_email,
              "Телефон контактного лица": Contact_person_phone_number,
              "Категория": "",
              "Время закрытия": "",
              "Инициатор": Initiator,
              "Группа": Group,
              "Краткое описание": Short_description,
              "Описание": Description,
              "Крайний срок": "",
              "Сделано": "",
              "Статус": "Новое",
              "Время выполнение": "",
              "Кто зарегистрировал": "",
              "Ключ": key,
              "Время новое": date_time(),
              "Приоретет": A_priority,
              "Решение": "",
              "Код выполнения": "",
              "Время последнего изменения": ""
            }
        }

        WritersJson(dict, name_file="incidents.json").main()

        return redirect(url_for("create_ticket_emulator.index"))

    if request.method == "GET":
        return render_template("create_ticket_emulator/index.html", key=key)
