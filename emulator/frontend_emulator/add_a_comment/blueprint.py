#!/usr/bin/env python3
# -*- 	coding: utf-8 -*-


from flask import request
from flask import Blueprint
from flask import render_template
from backend_emulator.date_time.return_date_time import date_time
from backend_emulator.working_with_JSON_files.read_in_json import ReadsJson
from backend_emulator.working_with_data_add_comment.work_data import WorkDataAddComment


add_comment = Blueprint("add_comment", __name__, template_folder="templates")

@add_comment.route("/", methods=["GET", "POST"])
def index():
    data_json = ReadsJson(name_file="incidents.json").main()

    if data_json is not False:
        list_key_ticket = list(data_json.keys())

        if request.method == "POST":
            value = request.form["drop_down"]
            commit = request.form["commit"]
            author = request.form["author"]

            dict_data = {
                    value: [
                            {
                                    "author": author,
                                    "commit": commit,
                                    "data": date_time()
                            }
                    ]
            }

            WorkDataAddComment(dict_data).main()

            return render_template("add_comment/index.html", drop_down_list=list_key_ticket)

        if request.method == "GET":
            return render_template("add_comment/index.html", drop_down_list=list_key_ticket)

    else:
        return render_template("no_ticket/index.html", data="Нет тикетов!")
