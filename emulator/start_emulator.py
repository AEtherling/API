#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from config.vars import host_emulator
from config.vars import port_emulator
from frontend_emulator import view_frontend_emulator
from frontend_emulator.app_frontend_emulator import app_frontend_emulator


def start_emulator_flask():
    app_frontend_emulator.run(host=host_emulator, port=port_emulator)


if __name__ == "__main__":
    start_emulator_flask()
