#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import json


class GetCommentJson:
    def __init__(self, data_json: bool or dict, key_id: str) -> (None or dict):
        if isinstance(data_json, bool):
            self.data_json = None
        if isinstance(data_json, dict):
            self.data_json = data_json
        self.key_id = key_id


    def return_str_response(self) -> str:
        list_response = []

        for data_json in self.data_json[self.key_id]:
            data_1 = data_json["author"]
            data_2 = data_json["data"]
            data_3 = data_json["commit"]

            data_string_1 = f'\"author\":\"{data_1}\",'
            data_string_2 = f'\"date\":\"{data_2}\",'
            data_string_3 = f'\"message\":\"[\\"{data_3}\\"]\"'

            data_string = f'{"{"}{data_string_1}{data_string_2}{data_string_3}{"}"}'

            list_response.append(data_string)

        all_data_string = f'"{self.key_id}":{list_response}'

        return all_data_string


    def return_response_prepared(self, str_response: str) -> str:
        return f'{"{"}{str_response}{"}"}'


    def return_dict_prepared_data_according_to_doc(self, response_prepared: str) -> dict:
        dict = {
                "Messages": [],
                "ReturnCode": 0,
                "ditMFSMAPI": {
                                "Action": "getProtocol",
                                "Filename": "Обращение",
                                "Key": self.key_id,
                                "ParamsNames": [
                                                "Полный протокол"
                                ],
                                "ParamsValues": [
                                                "true"
                                ],
                                "Response": [
                                                response_prepared
                                ],
                                "Status": "SUCCESS"
                }
        }

        return dict


    def convert_dict_to_json(self, dict_prepared_data_according_to_doc: dict) -> dict:
        json_dumps = json.dumps(dict_prepared_data_according_to_doc, sort_keys=True, ensure_ascii=False)
        json_loads = json.loads(json_dumps)
        return json_loads


    def main(self):
        if isinstance(self.data_json, dict):
            str_response = self.return_str_response()
            response_prepared = self.return_response_prepared(str_response)
            dict_prepared_data_according_to_doc = self.return_dict_prepared_data_according_to_doc(response_prepared)
            return self.convert_dict_to_json(dict_prepared_data_according_to_doc)
