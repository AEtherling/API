#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import json


class GetJson:
    def __init__(self, data_json: bool or dict) -> (None or dict):
        if isinstance(data_json, bool):
            self.data_json = None
        if isinstance(data_json, dict):
            self.data_json = data_json

    def return_list_response(self) -> list:
        list_response = []

        for key in self.data_json.keys():
            new_key = f'\"{key}\"'

            data_1 = self.data_json[key]["Имя файла"]
            data_2 = self.data_json[key]["Контактное лицо"]
            data_3 = self.data_json[key]["Email контактного лица"]
            data_4 = self.data_json[key]["Телефон контактного лица"]
            data_5 = self.data_json[key]["Категория"]
            data_6 = self.data_json[key]["Время закрытия"]
            data_7 = self.data_json[key]["Инициатор"]
            data_8 = self.data_json[key]["Группа"]
            data_9 = self.data_json[key]["Краткое описание"]
            data_10 = self.data_json[key]["Описание"]
            data_11 = self.data_json[key]["Крайний срок"]
            data_12 = self.data_json[key]["Сделано"]
            data_13 = self.data_json[key]["Статус"]
            data_14 = self.data_json[key]["Время выполнение"]
            data_15 = self.data_json[key]["Кто зарегистрировал"]
            data_16 = self.data_json[key]["Ключ"]
            data_17 = self.data_json[key]["Время новое"]
            data_18 = self.data_json[key]["Приоретет"]
            data_19 = self.data_json[key]["Решение"]
            data_20 = self.data_json[key]["Код выполнения"]
            data_21 = self.data_json[key]["Время последнего изменения"]

            data_string_1 = f'\"Имя файла\":\"{data_1}\",'
            data_string_2 = f'\"Контактное лицо\":\"{data_2}\",'
            data_string_3 = f'\"Email контактного лица\":\"{data_3}\",'
            data_string_4 = f'\"Телефон контактного лица\":\"{data_4}\",'
            data_string_5 = f'\"Категория\":\"{data_5}\",'
            data_string_6 = f'\"Время закрытия\":\"{data_6}\",'
            data_string_7 = f'\"Инициатор\":\"{data_7}\",'
            data_string_8 = f'\"Группа\":\"{data_8}\",'
            data_string_9 = f'\"Краткое описание\":\"{data_9}\",'
            data_string_10 = f'\"Описание\":\"{data_10}\",'
            data_string_11 = f'\"Крайний срок\":\"{data_11}\",'
            data_string_12 = f'\"Сделано\":\"{data_12}\",'
            data_string_13 = f'\"Статус\":\"{data_13}\",'
            data_string_14 = f'\"Время выполнение\":\"{data_14}\",'
            data_string_15 = f'\"Кто зарегистрировал\":\"{data_15}\",'
            data_string_16 = f'\"Ключ\":\"{data_16}\",'
            data_string_17 = f'\"Время новое\":\"{data_17}\",'
            data_string_18 = f'\"Приоретет\":\"{data_18}\",'
            data_string_19 = f'\"Решение\":\"{data_19}\",'
            data_string_20 = f'\"Код выполнения\":\"{data_20}\",'
            data_string_21 = f'\"Время последнего изменения\":\"{data_21}\"'

            all_data_string = f'{new_key}:{"{"}{data_string_1}{data_string_2}{data_string_3}{data_string_4}{data_string_5}{data_string_6}{data_string_7}{data_string_8}{data_string_9}{data_string_10}{data_string_11}{data_string_12}{data_string_13}{data_string_14}{data_string_15}{data_string_16}{data_string_17}{data_string_18}{data_string_19}{data_string_20}{data_string_21}{"}"}'

            list_response.append(all_data_string)

        return list_response

    def return_all_records(self, list_response: list) -> int:
        return len(list_response)

    def return_response_prepared(self, list_response: list, all_records: int) -> str:
        if all_records >= 2:
            return f"{'{'}{','.join(list_response)}{'}'}"
        else:
            for response in list_response:
                return f"{'{'}{response}{'}'}"

    def return_dict_prepared_data_according_to_doc(self, all_records: int, response_prepared: str) -> dict:
        dict = {
                "Messages": [],
                "ReturnCode": 0,
                "ditMFSMAPI": {
                                "Action": "getObjectList",
                                "Filename": "Обращение",
                                "ParamsNames": [
                                                "Поисковый запрос",
                                                "Расширенный ответ",
                                                "showEmptyFields"
                                ],
                                "ParamsValues": [
                                                "\"Поле: Направление\"=\"Образование\" and \"Поле: Приоритет\"=\"4\" and \"Поле: Время 'Зарегистрировано'\">\"10/12/21 19:00:00\" ",
                                                "true",
                                                "true"
                                ],
                                "Response": [
                                            "Найдены следующие объекты:",
                                            f"{response_prepared}",
                                            f"Всего записей: {all_records}",
                                            "Страница: 1",
                                            "Записей на странице: 1",
                                            "Всего страниц: 1"
                                ],
                                "Status": "SUCCESS"
                                }
                }

        return dict

    def convert_dict_to_json(self, dict_prepared_data_according_to_doc: dict) -> dict:
        json_dumps = json.dumps(dict_prepared_data_according_to_doc, sort_keys=True, ensure_ascii=False)
        json_loads = json.loads(json_dumps)
        return json_loads

    def main(self):
        if isinstance(self.data_json, dict):
            list_response = self.return_list_response()
            all_records = self.return_all_records(list_response)
            response_prepared = self.return_response_prepared(list_response, all_records)
            dict_prepared_data_according_to_doc = self.return_dict_prepared_data_according_to_doc(all_records, response_prepared)
            return self.convert_dict_to_json(dict_prepared_data_according_to_doc)
