#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import random
from uuid import uuid4
from backend_emulator.working_with_JSON_files.write_in_json import WritersJson


class GetTokenAttachment:
    def __init__(self, key: str, data_json: dict):
        self.key = key
        self.data_json = data_json


    def convert_second_to_uppercase(self, random_characters: str) -> str:
        result = ""

        for i in range(len(random_characters)):
            if i % 2 == 1:
                result += random_characters[i].upper()
            else:
                result += random_characters[i]

        return result


    def get_generate_attach_token(self) -> str:
        return uuid4().hex


    def get_answer_json(self, generate_attach_token: str) -> dict:
        answer_data_json = {
                            self.key: {
                                        "Messages": [],
                                        "ReturnCode": 0,
                                        "ditMFSMAPI": {
                                                        "Action": self.data_json[self.key]["ditMFSMAPI"]["Action"],
                                                        "AttachToken": generate_attach_token,
                                                        "Filename": self.data_json[self.key]["ditMFSMAPI"]["Filename"],
                                                        "Key": self.key,
                                                        "Status": "SUCCESS"
                                        }
                            }
        }

        return answer_data_json


    def save_token_attachment(self, answer_data_json: dict):
        WritersJson(answer_data_json, name_file="token_attachment.json").main()


    def main(self):
        generate_attach_token = self.get_generate_attach_token()
        answer_data_json = self.get_answer_json(generate_attach_token)
        self.save_token_attachment(answer_data_json)
