import requests, os, io
from flask import request


class AttachmentHandler:
    def __init__(self, dict_with_key: dict, file_data):
        self.dict_with_key = dict_with_key
        self.file_data = file_data


    def get_key_value_from_dict(self):
        value = self.dict_with_key['drop_down']
        return value


    def get_token_attachment(self):
        try:
            attachment_token = requests.post(request.host_url + '/get_token_attachment_DOC', self.dict_with_key).json()[self.get_key_value_from_dict()]['ditMFSMAPI']['AttachToken']
            return attachment_token
        except (requests.exceptions.RequestException) as e:
            print("An error occurred:", str(e))
            return None


    def process_file(self, headers):
        attachment_token = self.get_token_attachment()
        try:
            with io.BytesIO() as temp_file:
                file_data = self.file_data.read()
                if file_data is None or len(file_data) == 0:
                    temp_file.write(b'')
                else:
                    temp_file.write(file_data)

                temp_file.seek(0)

                request_to_attach = requests.post(
                    request.host_url + "add_attachment/" + attachment_token + '/attachments',
                    files={'file': (self.file_data.filename, temp_file)},
                    headers=headers
                )
                request_to_attach.raise_for_status()
                return request_to_attach

        except (IOError, requests.exceptions.RequestException) as e:
            print("An error occurred:", str(e))
            return None


    def request_to_add_attachment(self):
        headers = {
            'Content-Disposition': f'attachment;filename*=utf-8\'\'{self.file_data.filename.encode("utf-8")}'
        }
        self.process_file(headers)


    def main(self):
        self.request_to_add_attachment()
