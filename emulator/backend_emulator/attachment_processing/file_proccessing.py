import os, re, uuid, json, codecs
from backend_emulator.working_with_JSON_files.read_in_json import ReadsJson
from backend_emulator.attachment_processing.__init__ import current_directory, attachments_folder_name


class FileProccessing:
        def __init__(self, file_data, attach_token, header):
            self.file_data = file_data
            self.attach_token = attach_token
            self.header = header

        
        def directory_for_attachment(self, attachments_folder_name, ticket_id):
            if ticket_id is not None:
                path_to_directory_with_attachments = os.path.join(current_directory, attachments_folder_name, str(ticket_id))
                return(path_to_directory_with_attachments)
            else:
                return("The value of ticket_id is not found.")


        def filename_generation_if_exists(self, path_to_directory_with_attachments, filename):
            base_name, extension = os.path.splitext(filename)
            i = 1
            while os.path.exists(f'{path_to_directory_with_attachments}/{base_name}_{i}{extension}'):
                i += 1
            new_filename = f'{base_name}_{i}{extension}'
            return new_filename


        def extract_filename_from_header(self):
            filename_regex = r"filename\*=utf-8''(.+)"
            match = re.search(filename_regex, self.header)
            if match:
                filename = match.group(1)
                return filename
            else:
                return None


        def decode_filename(self):
            filename = self.extract_filename_from_header()
            filename = filename[2:-1].replace("'", "").encode('utf-8')
            decoded_filename = codecs.escape_decode(filename)[0].decode('utf-8')
            return decoded_filename


        def check_file(self):
            if self.file_data is None:
                return 'No file attached'
            data_json = ReadsJson(name_file="token_attachment.json").main()
            return data_json


        def check_attach_token(self):
            data_json = self.check_file()
            for key, value in data_json.items():
                if value["ditMFSMAPI"]["AttachToken"] == self.attach_token:
                    result_key = value["ditMFSMAPI"]["Key"]
                    break
                else:
                    result_key = None
            return result_key
        

        def save_file(self, file_path, file_name):
            with open(f'{file_path}/{file_name}', 'wb') as file:
                file.write(self.file_data.read())


        def save_attachment_to_fs(self):
            try:
                result_key = self.check_attach_token()
                filename = self.decode_filename()
                if not result_key:
                    return "No token"
                elif result_key:
                    attachment_ticket_key = self.directory_for_attachment(attachments_folder_name, result_key)
                    full_path_to_file = os.path.join(attachment_ticket_key, filename)
                    os.makedirs(attachment_ticket_key, exist_ok=True)
                    if not os.path.exists(full_path_to_file):
                        self.save_file(attachment_ticket_key, filename)
                    else:
                        new_filename = self.filename_generation_if_exists(attachment_ticket_key, filename)
                        self.save_file(attachment_ticket_key, new_filename)
            except Exception as error:
                print(error)


        def gen_href(self):
            cid = uuid.uuid4().hex
            return cid
        

        def get_size(self):
            result_key = self.check_attach_token()
            filename = self.decode_filename()
            attachment_ticket_key = self.directory_for_attachment(attachments_folder_name, result_key)
            full_path_to_file = os.path.join(attachment_ticket_key, filename)
            file_size = os.path.getsize(full_path_to_file)
            return(file_size)


        def create_response(self):
            href = self.gen_href()
            size = self.get_size()
            filename = self.decode_filename()

            response = {    
                "Messages": [],
                "ReturnCode": 0,
                "attachment": {
                    "href": f"cid:{href}",
                    "len": size,
                    "name": filename,
                    "type": "application/octet-stream",
                    "xmime:contentType": "application/octet-stream"
                }
            }

            response_json = json.dumps(response, indent=4, sort_keys=True, ensure_ascii=False)
            return response_json


        def main(self):
            self.save_attachment_to_fs()
            return self.create_response()