#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import json
from backend_emulator.working_with_JSON_files.return_abs_path_json_file import abs_path_json_file


class WritersJson:
    def __init__(self, arg: dict, name_file: str):
        if isinstance(arg, dict):
            self.dict = arg
            self.abs_path_json_file = abs_path_json_file(name_file)

    def write_json(self):
        with open(self.abs_path_json_file, "a") as file:
            json.dump(self.dict, file, indent=4, sort_keys=True, ensure_ascii=False)

    def add_json(self):
        with open(self.abs_path_json_file, "r") as file:
            json_data = json.load(file)
            json_data.update(self.dict)

        with open(self.abs_path_json_file, "w") as file:
            json.dump(json_data, file, indent=4, sort_keys=True, ensure_ascii=False)

    def main(self):
        if os.path.exists(self.abs_path_json_file) is True:
            self.add_json()
        else:
            self.write_json()
