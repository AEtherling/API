#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import json
from backend_emulator.working_with_JSON_files.return_abs_path_json_file import abs_path_json_file


class ReadsJson:
    def __init__(self, name_file: str):
        self.abs_path_json_file = abs_path_json_file(name_file)

    def read_json(self):
        with open(self.abs_path_json_file, "r") as file:
            json_data = json.load(file)

        return json_data

    def main(self) -> (dict or bool):
        if os.path.exists(self.abs_path_json_file) is True:
            json_data = self.read_json()

            return json_data
        else:
            return False
