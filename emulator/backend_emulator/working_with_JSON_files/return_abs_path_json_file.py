#!/sur/bin/env python3
# -*- coding: utf-8 -*-


import os


def abs_path_json_file(name_file: str) -> str:
    return f"{os.path.abspath('./')}/{name_file}"
