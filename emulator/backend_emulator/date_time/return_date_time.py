#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import datetime


def date_time() -> str:
    now_date_time = datetime.datetime.now()
    format_date_time = now_date_time.strftime("%d/%m/%y %H:%M:%S")
    return format_date_time
