#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import re
import json
from backend_emulator.working_with_JSON_files.return_abs_path_json_file import abs_path_json_file


class Generator:
    def __init__(self):
        self.abs_path_json_file = abs_path_json_file(name_file="incidents.json")

    def return_last_key(self) -> str:
        with open(self.abs_path_json_file, "r") as file:
            json_data = json.load(file)
            last_key = list(json_data.keys())[-1]
        return last_key

    def return_new_key(self, last_key: str) -> str:
        match = re.search(r'(\D+)(\d+)', last_key)
        prefix, number = match.groups()
        new_number = f"{int(number) + 1:0{len(number)}}"
        new_key = f"{prefix}{new_number}"
        return new_key

    def main(self) -> str:
        if os.path.exists(self.abs_path_json_file) is True:
            last_key = self.return_last_key()
            new_key = self.return_new_key(last_key)
            return new_key
        else:
            key = "IM00000001"
            return key
