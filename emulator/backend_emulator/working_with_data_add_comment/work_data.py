#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from backend_emulator.working_with_JSON_files.read_in_json import ReadsJson
from backend_emulator.working_with_JSON_files.write_in_json import WritersJson


class WorkDataAddComment:
    def __init__(self, dict_data: dict):
        if isinstance(dict_data, dict):
            self.dict_data_add_comment = dict_data


    def checking_for_file_presence(self) -> (bool or dict):
        return ReadsJson(name_file="protocol.json").main()


    def create_and_write_data_file_protocol_json(self, *args, re_entry=False):
        if re_entry is False:
            WritersJson(self.dict_data_add_comment, name_file="protocol.json").main()

        if re_entry is True:
            new_dict_data_json_protocol = args[0]
            WritersJson(new_dict_data_json_protocol, name_file="protocol.json").main()


    def find_non_matching_values(self, list_key_ticket_protocol_json: list, list_key_ticket_new_data: list):
        non_matching_list = [value for value in list_key_ticket_new_data if value not in list_key_ticket_protocol_json]

        if len(non_matching_list) == 0:
            return False
        else:
            return non_matching_list


    def supplementing_the_protocol(self, data_json_protocol: dict, list_key_ticket_new_data: list) -> dict:
        new_list_data_json_protocol = []

        key_ticket_new_data = list_key_ticket_new_data[0]
        list_data_new = self.dict_data_add_comment[key_ticket_new_data][0]
        list_data_json_protocol_key = data_json_protocol[key_ticket_new_data]

        for data_json_protocol_key in list_data_json_protocol_key:
            new_list_data_json_protocol.append(data_json_protocol_key)

        new_list_data_json_protocol.append(list_data_new)

        new_dict = {
                    key_ticket_new_data: new_list_data_json_protocol
        }

        return new_dict


    def main(self):
        data_json_protocol = self.checking_for_file_presence()

        if isinstance(data_json_protocol, bool):
            self.create_and_write_data_file_protocol_json()

        if isinstance(data_json_protocol, dict):
            list_key_ticket_protocol_json = list(data_json_protocol.keys())
            list_key_ticket_new_data = list(self.dict_data_add_comment.keys())

            non_matching_list = self.find_non_matching_values(list_key_ticket_protocol_json, list_key_ticket_new_data)

            if non_matching_list is not False:
                self.create_and_write_data_file_protocol_json()

            if non_matching_list is False:
                new_dict_data_json_protocol = self.supplementing_the_protocol(data_json_protocol, list_key_ticket_new_data)
                self.create_and_write_data_file_protocol_json(new_dict_data_json_protocol, re_entry=True)
